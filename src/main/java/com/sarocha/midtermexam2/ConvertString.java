/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.sarocha.midtermexam2;

import java.util.Scanner;

/**
 *
 * @author Sarocha
 */
public class ConvertString {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String S = kb.next();
        
        System.out.println("Convert String to Integer : ");
        int result = Integer.parseInt(S);
        System.out.println(result);
    }
}
